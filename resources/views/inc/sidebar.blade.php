    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">CMS ADMIN</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item @if (\Request::is('/')) active  @endif">
        <a class="nav-link" href="{{ url('/') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      
     

      <li class="nav-item 
        @if (\Request::is('post') || 
            \Request::is('post/*') ||
            \Request::is('category') || 
            \Request::is('category/*')) active 
        @endif">
        <a class="nav-link 
        @if (!\Request::is('post') &&
            !\Request::is('post/*') &&
            !\Request::is('category') &&
            !\Request::is('category/*')) collapsed
        @endif" href="#" data-toggle="collapse" data-target="#post" aria-expanded="true" aria-controls="post">
          <i class="fas fa-fw fa-edit"></i>
          <span>Post</span>
        </a>
        <div id="post" class="collapse 
        @if (\Request::is('post') || 
            \Request::is('post/*') ||
            \Request::is('category') ||
            \Request::is('category/*')) show  
        @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">            
            <a class="collapse-item 
            @if (\Request::is('post') || 
                \Request::is('post/edit/*')) active  
            @endif" href="{{url('post')}}">List</a>
            <a class="collapse-item 
            @if (\Request::is('category') || 
                \Request::is('category/*')) active  
            @endif" href="{{url('category')}}">Category</a>
            <a class="collapse-item 
            @if (\Request::is('post/create')) active  
            @endif" href="{{url('post/create')}}">New</a>
          </div>
        </div>
      </li>
  
    
      <li class="nav-item 
        @if (\Request::is('profile') ||
            \Request::is('user') ||
            \Request::is('user/*')) active 
        @endif">
        <a class="nav-link 
        @if (!\Request::is('profile') && 
            !\Request::is('user') && 
            !\Request::is('user/*')) collapsed
        @endif" href="#" data-toggle="collapse" data-target="#setting" aria-expanded="true" aria-controls="setting">
          <i class="fas fa-fw fa-cog"></i>
          <span>Administrator</span>
        </a>
        <div id="setting" class="collapse
          @if (\Request::is('profile') ||
              \Request::is('profile/*') ||
              \Request::is('user') ||
              \Request::is('user/*')) show  
          @endif" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">            
            <a class="collapse-item
            @if (\Request::is('profile')) active             
            @endif" href="{{url('profile')}}" href="{{url('profile')}}">Profile</a>
            <a class="collapse-item
            @if (\Request::is('user') ||
                \Request::is('user/*')) active  
            @endif" href="{{url('user')}}">List User</a>        
          </div>
        </div>
      </li>   

    </ul>

    <!-- End of Sidebar -->