@extends('layout')
@section('content')
		<!-- Page Heading -->
		  
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Edit Category</h1>           
		</div>
		<form action="{{url('category/edit/'.$single->id)}}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="row">			
		        <div class="col-lg-8">	         
					<div class="card shadow mb-4">
			        	<div class="card-header">
		                  <i class="fas fa-edit"></i> Edit Category
		                </div>
			            <div class="card-body">
			            	<div class="form-group">
			            		<label for="">Title</label>
			            		<input name="title" class="form-control" type="text" placeholder="Enter Title" value="{{$single->title}}">
			            	</div>
			            	@if($errors->has('title'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('title')}}
                                </div>
                            @endif
			            	<div class="mt-4">
			            		<a href="{{url('category')}}" class="btn btn-light btn-icon-split">
				                    <span class="icon text-gray-600">
				                      <i class="fas fa-arrow-left"></i>
				                    </span>
				                    <span class="text">Cancel</span>
			                  	</a>
			            		<button type="submit" class="btn btn-success btn-icon-split float-right">
		                          <span class="icon text-white-50">
		                            <i class="fas fa-save"></i>
		                          </span>
		                          <span class="text">Save</span>
		                        </button>
			            	</div>		            	
			            </div>
			        </div>
		    	</div>		    
			</div>
		</form>
@endsection