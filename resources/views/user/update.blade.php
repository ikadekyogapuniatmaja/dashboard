@extends('layout')
@section('content')
		<!-- Page Heading -->
		  
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Edit User</h1>           
		</div>
		<form action="{{url('user/edit/'.$single->id)}}" method="POST">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="row">			
		        <div class="col-lg-8">	         
					<div class="card shadow mb-4">
			        	<div class="card-header">
		                  <i class="fas fa-edit"></i> Edit User
		                </div>
			            <div class="card-body">
			            	<div class="form-group">
			            		<label for="">Name</label>
			            		<input name="name" class="form-control" type="text" placeholder="Enter Name" value="<?= empty(old('name')) ? $single->name :old('name') ?>">
			            	</div>	
			            	@if($errors->has('name'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('name')}}
                                </div>
                            @endif
                            <div class="form-group">
			            		<label for="">Email</label>
			            		<input name="email" class="form-control" type="email" placeholder="Enter Email" value="<?= empty(old('email')) ? $single->email :old('email') ?>">
			            	</div>	
			            	@if($errors->has('email'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('email')}}
                                </div>
                            @endif
                            <div class="form-group">
			            		<label for="">Password ( <i>input if you wanna modify</i> )</label>
			            		<input name="password" class="form-control" type="password" placeholder="Enter Password">
			            	</div>
			            	@if($errors->has('password'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('password')}}
                                </div>
                            @endif
			            	<div class="mt-4">
			            		<a href="{{url('user')}}" class="btn btn-light btn-icon-split">
				                    <span class="icon text-gray-600">
				                      <i class="fas fa-arrow-left"></i>
				                    </span>
				                    <span class="text">Cancel</span>
			                  	</a>
			            		<button type="submit" class="btn btn-success btn-icon-split float-right">
		                          <span class="icon text-white-50">
		                            <i class="fas fa-save"></i>
		                          </span>
		                          <span class="text">Save</span>
		                        </button>
			            	</div>		            	
			            </div>
			        </div>
		    	</div>		    
			</div>
		</form>
@endsection