@extends('layout')
@section('content')
		<!-- Page Heading -->
		  
		<div class="d-sm-flex align-items-center justify-content-between mb-4">
			<h1 class="h3 mb-0 text-gray-800">Profile</h1>           
		</div>		
		<div class="row">			
	        <div class="col-lg-8">	         
				<div class="card shadow mb-4">
		        	<div class="card-header">
	                  <i class="fas fa-edit"></i> Profile
	                </div>
		            <div class="card-body">
		            	
		            	<div class="form-group">
		            		<label for="">Name : </label>
		            		<span>{{$single->name}}</span>
		            	</div>		            
		            	<div class="form-group">
		            		<label for="">Email : </label>
		            		<span>{{$single->email}}</span>
		            	</div>		            
		            	<div class="mt-4">		            		
		            		<a href="{{url('user/edit/'.Auth::id())}}" class="btn btn-info btn-icon-split float-right">
	                          <span class="icon text-white-50">
	                            <i class="fas fa-save"></i>
	                          </span>
	                          <span class="text">Edit</span>
	                        </a>
		            	</div>		            	
		            </div>
		        </div>
	    	</div>		    
		</div>
	
@endsection