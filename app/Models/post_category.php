<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class post_category extends Model
{
    protected $table = 'post_category';
    protected $fillable = [
        'post_id', 'category_id'
    ]; 

}
