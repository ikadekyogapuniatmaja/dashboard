<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use DB;
use Hash;

class UsersController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        
        if (Auth::attempt($credentials)) {            
            return redirect()->intended('/');
        }
    }
    public function login()
    {
    	if (Auth::check()) {
            return redirect('/');
        }else{
            return view('login');
        }
    }
    public function logout(){        
        Auth::logout();
        return redirect('/');    
    }

    public function view()
    {
        return view('user.create');
    }

    public function create(Request $req)
    {
        $req->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'password' => 'required|min:5|max:255',
        ]);
        
        User::create([
            'name' => $req->input('name'),
            'email' => $req->input('email'),
            'password' => Hash::make($req->input('password')),
        ]);

        $req->session()->flash('success','Success Make User');
        return redirect('user');
    }

    public function edit($id)
    {
        $data['single'] = User::where('id',$id)->first();
        return view('user.update')->with($data);
    }
    public function profile()
    {
        $data['single'] = User::where('id',Auth::id())->first();
        return view('user.profile')->with($data);
    }
    public function update(Request $req,$id)
    {
        if (!empty($req->input('password'))) {
            $req->validate([
                'name' => 'required|max:255',
                'email' => 'required|max:255|email',
                'password' => 'min:5|max:255',
            ]);
        }else{
            $req->validate([
                'name' => 'required|max:255',
                'email' => 'required|max:255|email',
                'password' => 'max:255',
            ]);
        }
        $data = [
            'name' => $req->input('name'),
            'email' => $req->input('email'),         
        ];
        if (!empty($req->input('password'))) {
          $data['password'] = Hash::make($req->input('password'));
        }
        User::find($id)->update($data);

        $req->session()->flash('success','Success Update User');
        return redirect('user');
    }

    public function delete(Request $req)
    {
        User::find($req->input('id'))->delete();    
        if ($req->input('id') == Auth::id()) {
            Auth::logout();
            return redirect('/');  
        }
        $req->session()->flash('success','Success Delete User');
        return redirect('user');        
    }
    public function store(Request $req)
    {
        $data['list'] = User::orderBy('id','DESC')->paginate(25);
        return view('user.list')->with($data);

    }

}